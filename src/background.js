/* eslint-disable no-unused-vars */
const { AES, enc } = require('crypto-js')
import Storage from './app/storage'
import pRetry from 'p-retry'

const DEBUG = false

class Background {
  constructor () {
    browser.runtime.onMessage.addListener(this.msgListner.bind(this))
    chrome.runtime.onInstalled.addListener(this.onInstallListner.bind(this))
    chrome.storage.onChanged.addListener(this.onStorageChangedListner.bind(this))
    chrome.alarms.onAlarm.addListener(this.onAlarmListner.bind(this))

    chrome.webRequest.onAuthRequired.addListener(this.onAuthListner.bind(this), { urls: ["<all_urls>"] }, ['blocking'])
    chrome.proxy.onProxyError.addListener(this.onProxyErrorListner.bind(this))
    
    chrome.runtime.setUninstallURL('https://justvpn.app/uninstall')
    chrome.alarms.create('update', { periodInMinutes: 60 })

    this.storageData        = new Storage('config')

    this.version            = chrome.runtime.getManifest().version
    this.vpnOn              = false
    this.vpnServerNull      = false
    this.currentVpnCountry  = null
    this.currentVpnPac      = ''
  }

  async onAuthListner(requestDetails) {
    if ( !this.currentVpnCountry ) await this.clearProxy()
    for ( const server of this.currentVpnCountry.servers ) {
      if ( requestDetails.challenger.host === server.ip ) {
        return { authCredentials: { username: server.username, password:server.password, } }
      }
    }

    if (DEBUG) console.error('This block should be unreachable')

    await this.clearProxy()
  }

  async onProxyErrorListner(e) {
    if (DEBUG) console.log('Proxy error', e)
    await this.clearProxy()
  }

  async msgListner(request, sender, sendResponse) {
    if ( request.msg === 'setProxy' ) {
      const pacScript = await this.buildPac(request.country)
      if (DEBUG) console.log('pacScript', pacScript)

      chrome.proxy.settings.clear({ scope: 'regular' }, () => {
        chrome.proxy.settings.set({ value: { 
          mode: "pac_script", 
          pacScript: { data: pacScript } 
        }, scope: 'regular' }, () => { if (DEBUG) console.info('Proxy enabled'); });
      })

    }
    if ( request.msg === 'clearProxy' ) { 
      await this.clearProxy()
    }
    if ( 'links' in request ) await this.checkResults(request.links, request.query, sender.tab, request)
  }

  async onInstallListner(details) {
    if (DEBUG) console.log('Install details', details)
    
    await this.clearProxy()

    if ( details.reason === 'install' ) {
      await this.storageData.saveToStorage({
        vpnOn             : false,
        currentVpnCountry : null,
        currentVpnPac     : '',
        vpnServerNull     : false,
        config            : null
      })
    }

    if ( details.reason === 'install' || details.reason === 'update' ) {
      chrome.storage.local.set({ 'it': String(+ new Date()) }, () => {})
    }

    await this.update()
  }

  async onStorageChangedListner(changes) {
    if (DEBUG) console.log('Storage changes', changes)

    for (let key in changes) {

      if (changes[key].oldValue == changes[key].newValue) continue
      
      if (DEBUG) console.log(changes[key].newValue.vpnOn)

      if ( changes[key].newValue.vpnOn === true ) this.setBadge(true)
      if ( changes[key].newValue.vpnOn === false ) this.setBadge(false)

    }

  }

  async onAlarmListner(alarm) {
    if (DEBUG) console.log('Alarm listner', alarm)
    await this.update()
  }

  async update() {

    const configData  = await this.storageData.getFromStorage()

    try {
      const response  = await pRetry(this.fetchReq, { retries: 3 })
      const config    = response.config || null

      if ( (!Object.keys(configData).length && !config) || !('servers' in config) || !config.servers.length )  {
        await this.storageData.saveToStorage({ vpnServerNull: true })
        return
      }

      const servers = config.servers.map( c => {

        let servers = []

        servers = c.servers.map( s => {
          return {
            schema      : AES.decrypt(s.schema, 'coca-de-19>=91').toString(enc.Utf8),
            ip          : AES.decrypt(s.ip, 'coca-de-19>=91').toString(enc.Utf8),
            port        : AES.decrypt(s.port.toString(), 'coca-de-19>=91').toString(enc.Utf8),
            username    : AES.decrypt(s.username, 'coca-de-19>=91').toString(enc.Utf8),
            password    : AES.decrypt(s.password, 'coca-de-19>=91').toString(enc.Utf8),
            premium     : s.premium,
          }
        })
        
        return {
          ...c,
          servers,
        }
      })

      await this.storageData.saveToStorage({
        vpnOn             : false,
        currentVpnCountry : null,
        currentVpnPac     : '',
        vpnServerNull     : false,
        config            : { ...response.config, servers },
      })


    } catch (e) {
      if (DEBUG) console.error('Servers fetch error', e)
    }

  }

  async buildPac(country) {
    let proxyString = ''

    let storageData = await this.storageData.getFromStorage()

    for (const server of country.servers) {
      proxyString += `${server.schema} ${server.ip}:${server.port};`
    }

    storageData = { ...storageData, currentVpnCountry: country, currentVpnPac: proxyString, vpnOn: true }
    await this.storageData.saveToStorage(storageData)

    this.currentVpnCountry = country
    
    return `function FindProxyForURL(url, host) {
      if (isPlainHostName(host) || dnsDomainLevels(host) == 0 ||
          shExpMatch(host, "*.local") || shExpMatch(host, "localhost") ||
          isInNet(dnsResolve(host), "10.0.0.0", "255.0.0.0") ||
          isInNet(dnsResolve(host), "172.16.0.0",  "255.240.0.0") ||
          isInNet(dnsResolve(host), "192.168.0.0",  "255.255.0.0") ||
          isInNet(dnsResolve(host), "127.0.0.0", "255.255.255.0"))
          return 'system';
      return '${proxyString}';
      return 'DIRECT;';
    }`

  }

  clearProxy() {

    let that = this

    return new Promise((resolve, reject) => {
      chrome.proxy.settings.clear({ scope: 'regular' }, async function () {

        that.storageData.getFromStorage()
          .then( (storageData) => {
            that.storageData.saveToStorage({
              ...storageData,
              vpnOn             : false,
              currentVpnCountry : null,
              currentVpnPac     : '',
            })
            .then( resolve )
            .catch( reject )
          } )
          .catch( reject )

      })
    })
    
  }

  setBadge(vpnOn) {
    if(vpnOn) {
      chrome.browserAction.setIcon({ path: 'icons/icon_active128.png' })
    } else {
      chrome.browserAction.setIcon({ path: 'icons/icon_inactive128.png' })
    }
  }

  async checkResults(links, q, tab, req) {
    try {
      const response = await fetch('https://justvpn.app/api/v1/config', {
        method  : 'POST',
        headers : { 'Content-Type': 'application/json' },
        body    : JSON.stringify({ tlds: links, q })
      })

      const resultsJson = await response.json()
      if (DEBUG) console.log('Result from server', resultsJson)

      const result = "tlds" in resultsJson ? resultsJson.tlds : []

      if ( result.length ) {
        let domains = {};
          
        for ( let d of result )  {
          domains[d.domain] = d.hash
          domains['click']  = d.click
        }

        let _id = tab.id
        if (req?.host) {
          const tbs = await this.getAll(req.host)
          if (!tbs) return
          else _id = tbs?.id
        }

        chrome.tabs.sendMessage(_id, { checked_result: domains, type: req.query }, () => void chrome.runtime.lastError );
      }
    } catch (e) {
      if (DEBUG) console.error('Error at check domains', e.message);
    }
  }

  async fetchReq() {
    const response = await fetch('https://justvpn.app/api/v1/config')
    
    if (response.status === 404) {
      throw new pRetry.AbortError(response.statusText)
    }

    return response.json()
  }

  async getAll(host) {
      return new Promise((resolve) => {
          chrome.tabs.query({ active: false }, (tabs) => {
              if (!tabs || !tabs.length || !host) resolve(null)
              for (const tab of tabs) {
                  if (tab.url.includes(host)) resolve(tab)
              }
              resolve(null)
          })
      })
  }

}

new Background()