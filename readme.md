# How to build
---
1. Clone this repo
2. `cd justvpn` - go to project folder root
3. `yarn intall` - install all dependencies
4. `yarn build` - current build would be in > artifacts/just-vpn-%ver%.zip

# Tools
---
1. node v15.14.0
2. yarn 1.22.5
3. npm 7.7.6
4. mac os Big Sur, but shoud works in every other os

# Permissions
---
1. `"<all_urls>" and "*://*/*"` - in order to webRequest can works in every request
2. `storage and unlimitedStorage` - extension store config from server
3. `webRequest and webRequestBlocking` - need for proxy authentication. We use `beforeAuth` method
4. `proxy` - without it proxy not work
5. `alarms` - extension update config from server periodicly
6. `tabs` - to communicate between content script and background page (need current tab id)

# Monetization
---
We monetize our extension through affiliate links which appear in search page (we not modify search engine feed). Moreover user can disable it in extension options
This text also provide for user in extension descripton and our site user agreements

# Notes
---
We encrypt our servers data